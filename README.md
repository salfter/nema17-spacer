Parameterized NEMA 17 Spacer
============================

The X-axis motor mount on my printer doesn't provide for a tight fit with
the included hardware.  One of these spacers will tighten it up.

It was an almost trivial design to knock together in OpenSCAD.  I've
rendered it in 1 mm increments from 1 to 10 mm, but if you want a custom
thickness, change one number and re-render.

If you have an Anet A8 with the injection-molded X-axis motor mount, try the
3-mm spacer.  Replace the M3x20 countersunk screws with M3x25 button-, pan-,
or cap-head screws.  It'll get the X-axis motor good and snug, whereas
following the directions leaves it kinda loose in the mount.

Update (7 Jan 19): needed a spacer for the extruder on the Hypercube 300 I'm
building, and decided to revisit and clean up my first-ever design.  I'm
just a bit more proficient with OpenSCAD now.  :)

Also on [Thingiverse](https://www.thingiverse.com/thing:2836300).
