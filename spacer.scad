// NEMA 17 spacer plate
//
// Scott Alfter
// v2.0
// 7 Jan 19

$fn=90;

linear_extrude(2) // set thickness here
difference()
{
    minkowski()
    {
        square(31.3, center=true);
        circle(d=11);
    }
    translate([-15.5,-15.5])
        circle(d=3.4);
    translate([15.5,-15.5])
        circle(d=3.4);
    translate([-15.5,15.5])
        circle(d=3.4);
    translate([15.5,15.5])
        circle(d=3.4);
    circle(d=23);
}
